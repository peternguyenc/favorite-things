Welcome to the first Git exercise!
Completing these prompts will help you get more comfortable with
Git.

1. What is your favorite color?
My favorite color is blue
***
  Remember to:
    - add
    - commit
    - push
  your answer before answering the next question!
***

2. What is your favorite food?
My favorite food is sushi

3. Who is your favorite fictional character?
My favorite fictional character is spiderman

4. What is your favorite animal?
My favorite animal is a turtle.

5. What is your favorite programming language? (Hint: You can always say Python!!)
My favorite programming language is Python
